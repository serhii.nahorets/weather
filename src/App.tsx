import React from "react";
import { Filter, Forecast, Header, Weather } from "./components";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();

export const App: React.FC = () => {
    return (
        <main>
            <QueryClientProvider client={queryClient}>
                <Filter/>
                <Header/>
                <Weather/>
                <Forecast/>
            </QueryClientProvider>
        </main>
    );
};
