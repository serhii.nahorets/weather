import React from 'react';
import { DayType } from '../types/DayType';

interface Props {
    data: DayType;
}

export const Day: React.FC<Props> = ({ data }) => {
    const day = [
        'Воскресенье',
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота',
    ][new Date(data.day).getDay()];

    return (
        <div className={`day ${data.type}`}>
            <p>{day}</p>
            <span>{data.temperature}</span>
        </div>
    );
};
