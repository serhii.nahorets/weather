import React from 'react';

export const Header: React.FC = () => {
    return (
        <div className="head">
            <div className="icon cloudy"/>
            <div className="current-date">
                <p>Пятница</p>
                <span>29 ноября</span>
            </div>
        </div>
    );
};
