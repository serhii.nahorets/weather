import React from 'react';
import { Day } from "./";
import { useQuery } from "react-query";
import { DayType } from "../types/DayType";

export const Forecast: React.FC = () => {
    const { isLoading, error, data } = useQuery('forecastData', () =>
        fetch('https://lab.lectrum.io/rtx/api/v2/forecast?city=Kiev').then(res =>
            res.json()
        )
    )

    let forecastsJSX = <h3>Something went wrong...</h3>;
    if (isLoading) forecastsJSX = <h3>Loading...</h3>;
    else if (error) forecastsJSX = <h3>An error has occurred...</h3>;
    else if (Array.isArray(data?.data)) {
        forecastsJSX = data.data.slice(0, 7).map((datum: any) => {
            let type = '';

            // TODO: I'm not sure about the correctness of mapping
            switch (datum.values.weather) {
                case 'Drizzle':
                    type = 'cloudy';
                    break;
                case 'Flurries':
                    type = 'rainy';
                    break;
                case 'Light Snow':
                    type = 'rainy';
                    break;
                case 'Light Rain':
                    type = 'rainy';
                    break;
            }

            const forecast: DayType = {
                day: datum.date,
                humidity: datum.values.humidity,
                id: datum.id,
                type: type,
                temperature: Number((datum.values.temperature).toFixed(0)),
            };
            return <Day key={forecast.id} data={forecast}/>
        });
    }

    return (
        <div className="forecast">
            {forecastsJSX}
        </div>
    );
};
