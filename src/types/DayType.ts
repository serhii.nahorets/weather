export interface DayType {
    id: string;
    humidity: number;
    day: number;
    temperature: number;
    type: string;
}


